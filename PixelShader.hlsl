#define MAX_STEPS 100
#define MAX_DIST 100.0f
#define SURF_DIST 0.001f

//Define a mod as hlsl's works slightly differently
#define goodmod(x, y) ((x) - (y) * floor((x) / (y)))

cbuffer ScreenSizeBuffer : register(b1){
	float2 screenSize;
}

cbuffer TimeBuffer : register(b2){
	float time;
}

float2x2 rot(float angle){
	float s = sin(angle);
	float c = cos(angle);
	return float2x2(c, -s, s, c);
}

float opUnion(float distA, float distB){
	return min(distA, distB);	
}

float opIntersect(float distA, float distB){
	return max(distA, distB);
}

float opSubtract(float distA, float distB){
	return max(-distB, distA);	
}

float sdSphere(float3 pos, float3 spherePos, float sphereRadius){
	return length(spherePos - pos) - sphereRadius;
}

float sdBox(float3 pos, float3 boxPos, float3 boxSize){
	float3 q = abs(boxPos - pos) - boxSize;
	return length(max(q, 0.0f)) + min(max(q.x, max(q.y, q.z)), 0.0f);
}

float sdGyroid(float3 pos, float scale, float thickness, float bias){
	pos *= scale;
	return abs(dot(sin(pos), cos(pos.zxy)) + bias) / scale - thickness;
}

float getSceneDist(float3 pos){
	//float3 boxPos = pos;
	//boxPos.yz = mul(rot(time * 0.3f), boxPos.yz);
	//float box = sdBox(boxPos, float3(0, 0, 0), float3(1, 1, 1));
	
	float sphere = sdSphere(pos, float3(0, 0, 0), 1);
	
	pos.z += time * 0.1f;
	float gyroidA = sdGyroid(pos, 10.0f, 0.01f, -1.4f);
	float gyroidB = sdGyroid(pos, 10.0f, 0.02f, 0.2f);
	
	float gyroid = opUnion(gyroidA, gyroidB);

	return opIntersect(sphere, gyroid * 0.7f); //reduce gyroid step size to avoid over steps on thinner gyroids
}

float rayMarch(float3 origin, float3 direction){
	float dist = 0;
	
	for(int i = 0; i < MAX_STEPS; i++){
		float3 pos = origin + (direction * dist);
		float sceneDist = getSceneDist(pos);
		dist += sceneDist;
		if(dist > MAX_DIST || abs(sceneDist) < SURF_DIST){
			break;	
		}
	}
	
	return dist;
}

float3 getNormal(float3 pos){
	float dist = getSceneDist(pos);
	float2 epsilon = float2(0.001f, 0.0f);
	float3 normal = dist - float3(getSceneDist(pos - epsilon.xyy), getSceneDist(pos - epsilon.yxy), getSceneDist(pos - epsilon.yyx));
	return normalize(normal);
}

float3 getRayDir(float2 uv, float3 pos, float3 look, float zoom){
	float3 forward		= normalize(look - pos);
	float3 right		= normalize(cross(float3(0, 1, 0), forward));
	float3 up			= cross(forward, right);
	float3 c			= pos + forward * zoom;
	float3 i			= c + (uv.x * right) + (uv.y * up);
	float3 direction	= normalize(i - pos);
	return direction;
}

float4 main(float2 tex : TexCoord) : SV_Target{
	float2 pixelPos = float2(tex.x * screenSize.x, tex.y * screenSize.y);
	float2 uv = (pixelPos - 0.5f * screenSize) / screenSize.y;

	float2 rayRot = float2(-time * 50.0f, 100) / screenSize;
	
	float3 rayOrigin = float3(0, 0, -5);
	rayOrigin.yz = mul(rot(-rayRot.y * 3.14f + 1.0f), rayOrigin.yz);
	rayOrigin.xz = mul(rot(-rayRot.x * 6.2831f), rayOrigin.xz);
	
	float3 rayDirection = getRayDir(uv, rayOrigin, float3(0, 0, 0), 2.0f);
	
	float sceneDist = rayMarch(rayOrigin, rayDirection);
	
	float3 col = float3(0, 0, 0);
	
	if(sceneDist < MAX_DIST){
		float3 currentPos = rayOrigin + (rayDirection * sceneDist);
		float3 normal = getNormal(currentPos);
		
		//Diffuse lighting
		float diff = dot(normal, normalize(float3(1, 2, 3))) * 0.5f + 0.5f;
		col += diff * normal;
	}
	
	return float4(col, 1.0f);
}